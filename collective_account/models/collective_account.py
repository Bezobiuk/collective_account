################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2020 Ihor Bezobiuk (<bezoblachnyj@gmail.com>).
#
################################################################################

import logging


from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class CollectiveAccount(models.Model):
    _name = 'collective.account'
    _description = 'Collective Account'

    @api.model
    def _get_default_currency(self):
        return self.env.company.currency_id

    partner_id = fields.Many2one(
        'res.partner', readonly=True, required=True, index=True,
        string='Partner', ondelete='restrict')
    product_id = fields.Many2one(
        'product.product', readonly=True, required=True, index=True,
        ondelete='restrict')
    product_total_qty = fields.Float(
        string='Total Quantity', readonly=True, required=True, index=True,
        digits='Product Unit of Measure')
    product_uom_id = fields.Many2one(
        'uom.uom', related='product_id.uom_id', string='UOM')
    product_total_amount = fields.Monetary(
        string="Total Amount", readonly=True, required=True,
        currency_field='collective_currency_id')
    collective_currency_id = fields.Many2one(
        'res.currency', readonly=True, required=True, string='Currency',
        default=_get_default_currency)

    _sql_constraints = [
        ('product_partner_uniq', 'unique (product_id,partner_id)',
         'Can be only one product record for a partner!'
         ' It looks like a duplicate of the record tries to be created by'
         ' these parameters.')
    ]

    # TODO: Add fields (for future):
    # last_collected_source: M2O last invoice was collected,
    # las_collected_date: date of post last collected invoice.

    def name_get(self):
        return [
            (rec.id, "%s [%s]" % (rec.partner_id.name, rec.product_id.name))
            for rec in self]
