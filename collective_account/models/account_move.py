################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2020 Ihor Bezobiuk (<bezoblachnyj@gmail.com>).
#
################################################################################

import logging


from odoo import models, fields

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = "account.move"

    def _create_or_update_collective_account(self):
        self.ensure_one()
        CollectiveAccount = self.env['collective.account']
        for line in self.invoice_line_ids.filtered(
                lambda x: x.product_id.type == 'consu'):
            col_account = CollectiveAccount.search([
                ('partner_id', '=', line.partner_id.id),
                ('product_id', '=', line.product_id.id)], limit=1)
            if not col_account:
                CollectiveAccount.create({
                    'partner_id': line.partner_id.id,
                    'product_id': line.product_id.id,
                    'product_total_qty': line.quantity,
                    'product_total_amount': line.price_subtotal,
                })
            else:
                col_account.write({
                    'product_total_qty':
                        col_account.product_total_qty + line.quantity,
                    'product_total_amount':
                        col_account.product_total_amount + line.price_subtotal,
                })
        return True

    def action_post(self):
        res = super(AccountMove, self).action_post()
        for move in self:
            move._create_or_update_collective_account()
        return res
