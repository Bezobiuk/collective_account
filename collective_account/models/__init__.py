################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2020 Ihor Bezobiuk (<bezoblachnyj@gmail.com>).
#
################################################################################

from . import collective_account
from . import account_move
