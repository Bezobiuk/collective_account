################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2020 Ihor Bezobiuk (<bezoblachnyj@gmail.com>).
#
################################################################################

{
    'name': 'Collective Account',
    'summary': "Extends account module. Collects data about total quantity and"
               " a total amount for posted invoice products customer grouped.",
    'version': '13.0.0.0.1',
    'author': "Ihor Bezobiuk",
    'category': 'Extra Tools',
    'depends': [
        'contacts',
        'sale',
    ],

    'data': [
        'security/ir.model.access.csv',
        'views/collective_account.xml',
    ],

    'auto_install': True,
    'installable': True,
    'application': False,
}
