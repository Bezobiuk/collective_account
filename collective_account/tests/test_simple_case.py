################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2020 Ihor Bezobiuk (<bezoblachnyj@gmail.com>).
#
################################################################################

import logging

from psycopg2 import IntegrityError

from odoo.tools import mute_logger
from odoo.tests import tagged
from odoo.addons.sale.tests.test_sale_common import TestCommonSaleNoChart

_logger = logging.getLogger(__name__)


@tagged('post_install', '-at_install')
class TestCollectiveAccount(TestCommonSaleNoChart):

    @classmethod
    def setUpClass(cls):
        super(TestCollectiveAccount, cls).setUpClass()

        AccountMove = cls.env['account.move'].with_context(
            tracking_disable=True)
        cls.CollectiveAccount = cls.env['collective.account']

        cls.partner_1 = cls.env.ref('base.res_partner_12')
        cls.partner_2 = cls.env.ref('base.res_partner_2')

        cls.product_1 = cls.env.ref('product.consu_delivery_02')
        cls.product_2 = cls.env.ref('product.consu_delivery_01')
        cls.product_3 = cls.env.ref('product.product_product_1')  # service

        # set up accounts and journals
        cls.setUpAdditionalAccounts()
        cls.setUpAccountJournal()

        # create draft invoices
        cls.invoice_1 = AccountMove.create({
            'type': 'out_invoice',
            'partner_id': cls.partner_1.id,
            'invoice_line_ids': [
                (0, 0, {'product_id': cls.product_1.id,
                        'price_unit': 10.0,
                        'quantity': 1}),
            ]
        })

        cls.invoice_2 = AccountMove.create({
            'type': 'out_invoice',
            'partner_id': cls.partner_1.id,
            'invoice_line_ids': [
                (0, 0, {'product_id': cls.product_1.id,
                        'price_unit': 10.0,
                        'quantity': 2}),
            ]
        })

        cls.invoice_3 = AccountMove.create({
            'type': 'out_invoice',
            'partner_id': cls.partner_2.id,
            'invoice_line_ids': [
                (0, 0, {'product_id': cls.product_1.id,
                        'price_unit': 10.0,
                        'quantity': 4}),
            ]
        })

        cls.invoice_4 = AccountMove.create({
            'type': 'out_invoice',
            'partner_id': cls.partner_2.id,
            'invoice_line_ids': [
                (0, 0, {'product_id': cls.product_1.id,
                        'price_unit': 8.0,
                        'quantity': 2}),
                (0, 0, {'product_id': cls.product_2.id,
                        'price_unit': 6.0,
                        'quantity': 3}),
            ]
        })

        cls.invoice_5 = AccountMove.create({  # service
            'type': 'out_invoice',
            'partner_id': cls.partner_2.id,
            'invoice_line_ids': [
                (0, 0, {'product_id': cls.product_3.id,
                        'price_unit': 8.0,
                        'quantity': 2}),
            ]
        })

    def test_collective_account_consumable_product_01(self):

        # ensure collective.account empty
        col_acc = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc), 0)

        # post first invoice
        self.invoice_1.action_post()

        col_acc_1 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_1), 1)
        self.assertEqual(col_acc_1.partner_id.id, self.partner_1.id)
        self.assertEqual(col_acc_1.product_id.id, self.product_1.id)
        self.assertEqual(col_acc_1.product_total_qty, 1)
        self.assertEqual(col_acc_1.product_total_amount, 10)

        # post second invoice
        self.invoice_2.action_post()

        col_acc_2 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_2), 1)
        self.assertEqual(col_acc_2.partner_id.id, self.partner_1.id)
        self.assertEqual(col_acc_2.product_id.id, self.product_1.id)
        self.assertEqual(col_acc_2.product_total_qty, 3)
        self.assertEqual(col_acc_2.product_total_amount, 30)

        # post thirds invoice
        self.invoice_3.action_post()

        col_acc_3 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_3), 2)

        col_acc_3_partner_1 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_1.id)])
        self.assertEqual(len(col_acc_3_partner_1), 1)
        self.assertEqual(col_acc_3_partner_1.product_id.id, self.product_1.id)
        self.assertEqual(col_acc_3_partner_1.product_total_qty, 3)
        self.assertEqual(col_acc_3_partner_1.product_total_amount, 30)

        col_acc_3_partner_2 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_2.id)])
        self.assertEqual(len(col_acc_3_partner_2), 1)
        self.assertEqual(col_acc_3_partner_2.product_id.id, self.product_1.id)
        self.assertEqual(col_acc_3_partner_2.product_total_qty, 4)
        self.assertEqual(col_acc_3_partner_2.product_total_amount, 40)

    def test_collective_account_consumable_product_02(self):
        # ensure collective.account empty
        col_acc = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc), 0)

        # post three invoices
        self.invoice_1.action_post()
        self.invoice_2.action_post()
        self.invoice_3.action_post()

        col_acc_3 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_3), 2)

        col_acc_3_partner_1 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_1.id)])
        self.assertEqual(len(col_acc_3_partner_1), 1)
        self.assertEqual(col_acc_3_partner_1.product_id.id,
                         self.product_1.id)
        self.assertEqual(col_acc_3_partner_1.product_total_qty, 3)
        self.assertEqual(col_acc_3_partner_1.product_total_amount, 30)

        col_acc_3_partner_2 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_2.id)])
        self.assertEqual(len(col_acc_3_partner_2), 1)
        self.assertEqual(col_acc_3_partner_2.product_id.id,
                         self.product_1.id)
        self.assertEqual(col_acc_3_partner_2.product_total_qty, 4)
        self.assertEqual(col_acc_3_partner_2.product_total_amount, 40)

        # post last invoice with two products
        self.invoice_4.action_post()

        col_acc_4 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_4), 3)

        col_acc_4_partner_1 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_1.id)])
        self.assertEqual(len(col_acc_4_partner_1), 1)
        self.assertEqual(col_acc_4_partner_1.product_id.id,
                         self.product_1.id)
        self.assertEqual(col_acc_4_partner_1.product_total_qty, 3)
        self.assertEqual(col_acc_4_partner_1.product_total_amount, 30)

        col_acc_4_partner_2 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_2.id)])
        self.assertEqual(len(col_acc_4_partner_2), 2)

        col_acc_4_partner_2_product_1 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_2.id),
             ('product_id', '=', self.product_1.id)])
        self.assertEqual(len(col_acc_4_partner_2_product_1), 1)
        self.assertEqual(col_acc_4_partner_2_product_1.product_total_qty, 6)
        self.assertEqual(col_acc_4_partner_2_product_1.product_total_amount, 56)

        col_acc_4_partner_2_product_2 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_2.id),
             ('product_id', '=', self.product_2.id)])
        self.assertEqual(len(col_acc_4_partner_2_product_2), 1)
        self.assertEqual(col_acc_4_partner_2_product_2.product_total_qty, 3)
        self.assertEqual(col_acc_4_partner_2_product_2.product_total_amount, 18)

    def test_collective_account_service_product(self):
        # ensure collective.account empty
        col_acc = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc), 0)

        # post three invoices
        self.invoice_1.action_post()
        self.invoice_2.action_post()
        self.invoice_3.action_post()

        col_acc_3 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_3), 2)

        col_acc_3_partner_1 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_1.id)])
        self.assertEqual(len(col_acc_3_partner_1), 1)
        self.assertEqual(col_acc_3_partner_1.product_id.id,
                         self.product_1.id)
        self.assertEqual(col_acc_3_partner_1.product_total_qty, 3)
        self.assertEqual(col_acc_3_partner_1.product_total_amount, 30)

        col_acc_3_partner_2 = self.CollectiveAccount.search(
            [('partner_id', '=', self.partner_2.id)])
        self.assertEqual(len(col_acc_3_partner_2), 1)
        self.assertEqual(col_acc_3_partner_2.product_id.id,
                         self.product_1.id)
        self.assertEqual(col_acc_3_partner_2.product_total_qty, 4)
        self.assertEqual(col_acc_3_partner_2.product_total_amount, 40)

        # post service invoice
        self.invoice_5.action_post()

        col_acc_5 = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc_5), 2)

        col_acc_3_product_3 = self.CollectiveAccount.search(  # service
            [('product_id', '=', self.product_3.id)])

        self.assertEqual(len(col_acc_3_product_3), 0)

    def test_collective_account_uniq(self):
        # ensure collective.account empty
        col_acc = self.CollectiveAccount.search([])
        self.assertEqual(len(col_acc), 0)

        # post three invoices
        self.invoice_1.action_post()

        # create the same product partner line
        with mute_logger('odoo.sql_db'):
            with self.assertRaises(IntegrityError):
                with self.cr.savepoint():
                    self.CollectiveAccount.create({
                        'partner_id': self.partner_1.id,
                        'product_id': self.product_1.id,
                        'product_total_qty': 0,
                        'product_total_amount': 0,
                    })
