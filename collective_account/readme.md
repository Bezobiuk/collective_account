Collective Account
==================

Extends account module.
Collects data about total quantity and a total amount for 
posted invoice products customer grouped.

Have to be set up journals or minimal chart of account for usage.
